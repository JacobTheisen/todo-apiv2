# Todo REST API 2.0

A simple Todo REST API with Node and Express.

### Instructions

1. Download the project:

```
git clone https://gitlab.com/ntnu-dcst2002/todo-api-v2.git
```

2. Install dependencies with npm:

```
cd todo-api-v2
npm install
```

3. Run the API:

```
npm run start
```